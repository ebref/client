import { Component, OnInit } from '@angular/core';
import { FormBuilder , FormGroup } from '@angular/forms'; 
import { Validators } from '@angular/forms';
import { adminService } from '../services/admin.service';
import {MatFormFieldModule} from '@angular/material/form-field';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  
  adForm:FormGroup;

  constructor(private adminService:adminService  ,private FormsBuilder:FormBuilder) { }

  ngOnInit() {
    this.initForm();
  }


  initForm(){
    this.adForm = this.FormsBuilder.group({
      username:['',Validators.required],
      password:['',Validators.required]
    })
  }

  onSubmitForm(){
    const value = this.adForm.value;
    console.log(value);
    this.adminService.login(value).subscribe(
      () => console.log('loggued') ,
       error => {
         console.log(error);
       }
    )
  }
}
